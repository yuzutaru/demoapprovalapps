package com.yuzu.skd.approvalapps.model;

import com.yuzu.skd.approvalapps.data.DaoSession;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */
public class BaseModel {
    protected final DaoSession mDaoSession;

    public BaseModel(DaoSession daoSession) {
        mDaoSession = daoSession;
    }
}
