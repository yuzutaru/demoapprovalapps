package com.yuzu.skd.approvalapps.helper;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */
public class Constant {
    public static final int TIMEOUT_HTTP                = 60;
    public static String URL_API                        = "http://10.101.1.50:8083/"; //Dev

    public static String API_LOGIN                      = "official/emp/login";
    public static String DATABASE_NAME                  = "approvalapps_DB";
}
