package com.yuzu.skd.approvalapps.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */

public class Login extends LoginParent implements Parcelable {
    @SerializedName("status")
    @Expose
    @Nullable
    private int status;

    @SerializedName("message")
    @Expose
    @Nullable
    private String message;

    @SerializedName("totalRecords")
    @Expose
    @Nullable
    private int totalRecords;

    protected Login(Parcel in) {
        status = in.readInt();
        message = in.readString();
        totalRecords = in.readInt();
    }

    public static final Creator<Login> CREATOR = new Creator<Login>() {
        @Override
        public Login createFromParcel(Parcel in) {
            return new Login(in);
        }

        @Override
        public Login[] newArray(int size) {
            return new Login[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeString(message);
        dest.writeInt(totalRecords);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }
}
