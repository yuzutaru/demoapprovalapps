package com.yuzu.skd.approvalapps.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.reactivex.annotations.Nullable;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */
public class LoginParent {
    @SerializedName("contents")
    @Expose
    @Nullable
    private LoginData data;

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }
}
