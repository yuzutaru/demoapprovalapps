package com.yuzu.skd.approvalapps.ui.presenter.contract.login;

import com.yuzu.skd.approvalapps.data.Login;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */
public class LoginActivityContract {
    public interface LoginView {
        void showProgressBar();
    }

    public interface LoginActionListener {
        void login(String user, String pass);
        void loginResponse(Login login);
    }
}
