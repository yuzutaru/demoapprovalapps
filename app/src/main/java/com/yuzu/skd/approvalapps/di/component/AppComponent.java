package com.yuzu.skd.approvalapps.di.component;

import com.yuzu.skd.approvalapps.module.AppModule;
import com.yuzu.skd.approvalapps.module.NetworkModule;
import com.yuzu.skd.approvalapps.ui.MainComponent;
import com.yuzu.skd.approvalapps.ui.module.BaseModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */


@Singleton
@Component(
        modules = {
                AppModule.class,
                NetworkModule.class
        }
)
public interface AppComponent {
    MainComponent plus(BaseModule baseModule);
}
