package com.yuzu.skd.approvalapps;

import android.app.Application;

import com.yuzu.skd.approvalapps.di.component.AppComponent;
import com.yuzu.skd.approvalapps.di.component.DaggerAppComponent;
import com.yuzu.skd.approvalapps.module.AppModule;
import com.yuzu.skd.approvalapps.module.NetworkModule;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */
public class ApprovalAppsApplication extends Application {
    private AppComponent appComponent = createAppComponent();
    private static ApprovalAppsApplication instance;

    public static ApprovalAppsApplication get() {
        return instance;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        instance = this;
    }

    protected AppComponent createAppComponent() {
        return appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
