package com.yuzu.skd.approvalapps.ui.module;

import android.database.sqlite.SQLiteDatabase;

import com.yuzu.skd.approvalapps.ApprovalAppsApplication;
import com.yuzu.skd.approvalapps.data.DaoMaster;
import com.yuzu.skd.approvalapps.data.DaoSession;
import com.yuzu.skd.approvalapps.di.scope.ActivityScope;
import com.yuzu.skd.approvalapps.helper.Constant;
import com.yuzu.skd.approvalapps.model.LoginModel;
import com.yuzu.skd.approvalapps.network.NetworkService;
import com.yuzu.skd.approvalapps.repository.MainRepository;
import com.yuzu.skd.approvalapps.ui.presenter.presenter.login.LoginActivityPresenter;
import com.yuzu.skd.approvalapps.ui.view.page.login.LoginActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */

@Module
public class BaseModule {
    private LoginActivity loginActivity;

//    ============================== Constructor ==========================

    public BaseModule(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
    }

//    ============================== Provider ==========================

    @Provides
    @ActivityScope
    DaoSession provideDaoSession() {
        String dbName = Constant.DATABASE_NAME;
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(ApprovalAppsApplication.get(), dbName);
        SQLiteDatabase db = devOpenHelper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);

        return daoMaster.newSession();
    }

    @Provides
    @ActivityScope
    MainRepository provideMainRepository(NetworkService networkService) {
        return new MainRepository(networkService);
    }

    @Provides
    @ActivityScope
    LoginModel provideLoginModel(DaoSession daoSession) {
        return new LoginModel(daoSession);
    }

    @Provides
    @ActivityScope
    LoginActivity provideLoginActivity() {
        return loginActivity;
    }

    @Provides
    @ActivityScope
    LoginActivityPresenter provideLoginActivityPresenter(MainRepository mainRepository, LoginModel loginModel) {
        return new LoginActivityPresenter(mainRepository, loginModel);
    }
}
