package com.yuzu.skd.approvalapps.repository;

import com.yuzu.skd.approvalapps.network.NetworkService;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */

public class BaseRepository {
    protected NetworkService networkService;

    public BaseRepository(NetworkService networkService) {
        this.networkService = networkService;
    }
}
