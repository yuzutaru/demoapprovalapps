package com.yuzu.skd.approvalapps.network;

import com.yuzu.skd.approvalapps.data.Login;

import io.reactivex.Flowable;
import retrofit2.http.Field;
import retrofit2.http.Url;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */
public interface NetworkService {
    /**
     * POST Login
     * */
    Flowable<Login> getLogin(@Url String url,
                             @Field("password") String pass,
                             @Field("username") String user);
}
