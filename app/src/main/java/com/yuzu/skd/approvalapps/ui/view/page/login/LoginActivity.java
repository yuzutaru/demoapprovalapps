package com.yuzu.skd.approvalapps.ui.view.page.login;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.yuzu.skd.approvalapps.ApprovalAppsApplication;
import com.yuzu.skd.approvalapps.R;
import com.yuzu.skd.approvalapps.ui.module.BaseModule;
import com.yuzu.skd.approvalapps.ui.presenter.contract.login.LoginActivityContract;
import com.yuzu.skd.approvalapps.ui.presenter.presenter.login.LoginActivityPresenter;
import com.yuzu.skd.approvalapps.ui.view.base.BaseViewActivity;
import com.yuzu.skd.approvalapps.ui.view.custom.widget.ProgressDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseViewActivity implements LoginActivityContract.LoginView {
    private String LOG_TAG = "LoginActivity";
    private LoginActivityContract.LoginActionListener mActionListener;
    private Context mContext;

    @Inject
    LoginActivityPresenter loginActivityPresenter;

    @BindView(R.id.username)
    EditText username;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.login_btn)
    Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_fragment);
        ButterKnife.bind(this);

        mContext = getApplicationContext();
        setupActivityComponent();
        mActionListener = loginActivityPresenter;
        loginActivityPresenter.setView(this, mContext);
    }

    private void setupActivityComponent() {
        ApprovalAppsApplication.get()
                .getAppComponent()
                .plus(new BaseModule(this))
                .inject(this);
    }

    @OnClick(R.id.login_btn)
    public void loginBtnOnClick() {}

    @Override
    public void showProgressBar() {
        ProgressDialog.showDialog(this, "Loading ", "Please wait", false);
    }
}
