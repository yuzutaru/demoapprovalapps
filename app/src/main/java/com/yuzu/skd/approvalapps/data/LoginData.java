package com.yuzu.skd.approvalapps.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;

import io.reactivex.annotations.Nullable;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */

@Entity
public class LoginData implements Parcelable {
    @SerializedName("kodeKaryawan")
    @Expose
    @Nullable
    private String kodeKaryawan;

    @SerializedName("namaKaryawan")
    @Expose
    @Nullable
    private String namaKaryawan;

    @SerializedName("password")
    @Expose
    @Nullable
    private String password;

    @SerializedName("username")
    @Expose
    @Nullable
    private String username;

    protected LoginData(Parcel in) {
        kodeKaryawan = in.readString();
        namaKaryawan = in.readString();
        password = in.readString();
        username = in.readString();
    }

    @Generated(hash = 279058857)
    public LoginData(String kodeKaryawan, String namaKaryawan, String password,
            String username) {
        this.kodeKaryawan = kodeKaryawan;
        this.namaKaryawan = namaKaryawan;
        this.password = password;
        this.username = username;
    }

    @Generated(hash = 1578814127)
    public LoginData() {
    }

    public static final Creator<LoginData> CREATOR = new Creator<LoginData>() {
        @Override
        public LoginData createFromParcel(Parcel in) {
            return new LoginData(in);
        }

        @Override
        public LoginData[] newArray(int size) {
            return new LoginData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(kodeKaryawan);
        dest.writeString(namaKaryawan);
        dest.writeString(password);
        dest.writeString(username);
    }

    public String getKodeKaryawan() {
        return this.kodeKaryawan;
    }

    public void setKodeKaryawan(String kodeKaryawan) {
        this.kodeKaryawan = kodeKaryawan;
    }

    public String getNamaKaryawan() {
        return this.namaKaryawan;
    }

    public void setNamaKaryawan(String namaKaryawan) {
        this.namaKaryawan = namaKaryawan;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
