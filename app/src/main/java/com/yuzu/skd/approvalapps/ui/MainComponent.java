package com.yuzu.skd.approvalapps.ui;

import com.yuzu.skd.approvalapps.di.scope.ActivityScope;
import com.yuzu.skd.approvalapps.ui.module.BaseModule;
import com.yuzu.skd.approvalapps.ui.view.page.login.LoginActivity;

import dagger.Subcomponent;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */

@ActivityScope
@Subcomponent(
        modules = BaseModule.class
)

public interface MainComponent {
    LoginActivity inject(LoginActivity loginActivity);
}
