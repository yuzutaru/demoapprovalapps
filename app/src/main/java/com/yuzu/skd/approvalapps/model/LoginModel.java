package com.yuzu.skd.approvalapps.model;

import com.yuzu.skd.approvalapps.data.DaoSession;
import com.yuzu.skd.approvalapps.data.LoginData;
import com.yuzu.skd.approvalapps.data.LoginDataDao;

import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

/**
 * Created by Yustar Pramudana on 11/12/2018.
 */
public class LoginModel extends BaseModel {
    private LoginDataDao mLoginDataDao;

    public LoginModel(DaoSession daoSession) {
        super(daoSession);
        mLoginDataDao = daoSession.getLoginDataDao();
    }

    @NonNull
    public void insertLoginData(LoginData loginData) {
        mLoginDataDao.insertOrReplace(loginData);
    }

    @Nullable
    public void deleteLoginData() {
        mLoginDataDao.deleteAll();
    }

    @Nullable
    public List<LoginData> getAllLoginData() {
        return mLoginDataDao.loadAll();
    }
}
