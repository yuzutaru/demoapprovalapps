package com.yuzu.skd.approvalapps.ui.view.custom.widget;


import android.content.Context;

public class ProgressDialog {
    private static android.app.ProgressDialog dialog;
//    private static Dialog dialog;

    public static void showDialog(Context context, String title, String message, Boolean cancelAble){
        if(dialog == null){
            dialog = android.app.ProgressDialog.show(context, title, message);
//            dialog = new android.app.ProgressDialog(context);
//            dialog = android.app.ProgressDialog.show(context, null, null);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(cancelAble);
            dialog.show();
        }

        if(!dialog.isShowing()){
            dialog.show();
        }
    }

//    public static void showDialog(Context context, String title, String message, Boolean cancelAble){
//        dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.show();
//    }

    public static void removeDialog(){
        if(dialog != null){
            if(dialog.isShowing()){
                dialog.dismiss();
                dialog = null;
            }
        }
    }

}
