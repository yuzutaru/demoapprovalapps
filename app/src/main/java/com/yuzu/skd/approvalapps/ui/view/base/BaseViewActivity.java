package com.yuzu.skd.approvalapps.ui.view.base;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.yuzu.skd.approvalapps.R;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */
public class BaseViewActivity extends AppCompatActivity {
    private String LOG_TAG = "BaseViewActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

    /************************
     * Go To Activity
     * simple go to activity
     * **********************/
    public void goToActivity(Class<?> cls, boolean finish, Bundle bundle) {
        Intent intent = new Intent(BaseViewActivity.this, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (bundle != null)
            intent.putExtras(bundle);
        startActivity(intent);
        if (finish)
            finish();
    }

    public void showDialogInfo(String tittle, String description, boolean onClick) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            final AlertDialog alertDialog = builder.create();

            builder.setMessage(description)
                    .setTitle(tittle)
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

            builder.show();

        } catch (Exception e) {
            showAlert(description);
        }
    }

    public void showAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
