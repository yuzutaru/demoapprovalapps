package com.yuzu.skd.approvalapps.ui.presenter.presenter.login;

import android.content.Context;
import android.util.Log;

import com.yuzu.skd.approvalapps.data.Login;
import com.yuzu.skd.approvalapps.data.LoginData;
import com.yuzu.skd.approvalapps.helper.Constant;
import com.yuzu.skd.approvalapps.model.LoginModel;
import com.yuzu.skd.approvalapps.repository.MainRepository;
import com.yuzu.skd.approvalapps.ui.presenter.contract.login.LoginActivityContract;
import com.yuzu.skd.approvalapps.ui.view.base.BaseViewActivity;

import io.reactivex.subscribers.ResourceSubscriber;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */
public class LoginActivityPresenter extends BaseViewActivity implements LoginActivityContract.LoginActionListener{
    private String LOG_TAG = "LoginPresenter";
    private LoginActivityContract.LoginView mView;
    private Context mContext;
    private MainRepository mMainRepository;
    private LoginModel mLoginModel;

    public LoginActivityPresenter(MainRepository mainRepository, LoginModel loginModel) {
        mMainRepository = mainRepository;
        mLoginModel = loginModel;
    }

    public void setView(LoginActivityContract.LoginView view, Context context) {
        mView = view;
        mContext = context;
    }

    @Override
    public void login(String user, String pass) {
        mMainRepository.getLogin(Constant.API_LOGIN, pass, user)
                .subscribe(new ResourceSubscriber<Login>() {
                    @Override
                    public void onNext(Login login) {
                        Log.d(LOG_TAG, "login onNext");
                        loginResponse(login);
                    }

                    @Override
                    public void onError(Throwable t) {
                        if (t != null) {
                            Log.d(LOG_TAG, "login onError");
                            showDialogInfo("Login", "login onError", true);
                        } else {
                            Log.d(LOG_TAG, "login onError null");
                            showDialogInfo("Login", "login onError null", true);
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void loginResponse(Login login) {
        try {
            int status = login.getStatus();
            String message = login.getMessage();
            int totalRecords = login.getTotalRecords();

            if (status == 0) {
                if (message.equals("Data OK")) {
                    saveLogin(login.getData());
                    showDialogInfo("Login", "Login Success", true);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            showDialogInfo("Login", e.getMessage(), true);
        }
    }

    private void saveLogin(LoginData loginData) {
        mLoginModel.insertLoginData(loginData);
    }
}
