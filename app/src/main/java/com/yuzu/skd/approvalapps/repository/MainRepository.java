package com.yuzu.skd.approvalapps.repository;

import com.yuzu.skd.approvalapps.data.Login;
import com.yuzu.skd.approvalapps.network.NetworkService;

import io.reactivex.Flowable;

/**
 * Created by Yustar Pramudana on 11/9/2018.
 */
public class MainRepository extends BaseRepository {
    public MainRepository(NetworkService networkService) {
        super(networkService);
    }

    /**
     * Login
     * */
    public Flowable<Login> getLogin(String url, String pass, String user) {
        return networkService.getLogin(url, pass, user);
    }
}
